#!flask/bin/python
from flask import Blueprint, request, jsonify
from sense_hat import SenseHat

imu_sensor = Blueprint('imu_sensor', __name__)
hat = SenseHat()

@imu_sensor.route('/hat/imu/set_imu_config', methods=['POST'])
def get_humidity():
    j = request.get_json()
    compass_enabled = True if j['compass_enabled'] else False
    gyro_enabled = True if j['gyro_enabled'] else False
    accel_enabled = True if j['accel_enabled'] else False
    hat.set_imu_config(compass_enabled, gyro_enabled, accel_enabled)
    return '', 200

@imu_sensor.route('/hat/imu/get_all')
def get_all():
    rsp = {}
    rsp['orientation'] = hat.get_orientation()
    rsp['gyroscope'] = hat.get_gyroscope()
    rsp['accelerometer'] = hat.get_accelerometer()
    rsp['compass'] = hat.get_compass()
    return jsonify(rsp), 200

@imu_sensor.route('/hat/imu/get_orientation_<string:angle>')
@imu_sensor.route('/hat/imu/get_orientation')
def get_orientation(angle='degrees'):
    if (angle == 'radians'):
        return jsonify(hat.get_orientation_radians())
    return jsonify(hat.get_orientation_degrees())

@imu_sensor.route('/hat/imu/get_compass', methods=['GET'])
def get_compass():
    return jsonify(hat.get_compass())

@imu_sensor.route('/hat/imu/get_gyroscope', methods=['GET'])
def get_gyroscope():
    return jsonify(hat.get_gyroscope())

@imu_sensor.route('/hat/imu/get_accelerometer', methods=['GET'])
def get_accelerometer():
    return jsonify(hat.get_accelerometer())