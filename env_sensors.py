#!flask/bin/python
from flask import Blueprint, request, jsonify
from sense_hat import SenseHat

env_sensors = Blueprint('env_sensors', __name__)
hat = SenseHat()

@env_sensors.route('/hat/env/get_all', methods=['GET'])
def get():
    rsp = {'temperature': hat.get_temperature(), 'humidity': hat.get_humidity(), 'pressure': hat.get_pressure() }
    return jsonify(rsp), 200

@env_sensors.route('/hat/env/get_humidity', methods=['GET'])
def get_humidity():
    return jsonify(hat.get_humidity()), 200

@env_sensors.route('/hat/env/get_temperature', methods=['GET'])
def get_temperature():
    return jsonify(hat.get_temperature()), 200

@env_sensors.route('/hat/env/get_pressure', methods=['GET'])
def get_pressure():
    return jsonify(hat.get_pressure()), 200