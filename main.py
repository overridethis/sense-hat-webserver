#!flask/bin/python
from flask import Flask, request, jsonify
from sense_hat import SenseHat

# Blueprints
from led_matrix import led_matrix
from env_sensors import env_sensors
from imu_sensor import imu_sensor

app = Flask(__name__)
hat = SenseHat()

app.register_blueprint(led_matrix)
app.register_blueprint(env_sensors)
app.register_blueprint(imu_sensor)

@app.route('/hat')
def index():
    return "SenseHat API!", 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)