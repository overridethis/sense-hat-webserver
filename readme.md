# SenseHat WebServer
Simple Python [Flask](http://flask.pocoo.org/) based Web API that allows you to interact with the SenseHat using simple HTTP protocol. Only Sensors that make sense are exposed thru the Web API.

## Sensors
* LED Matrix
* Environmental Sensors
* IMU Sensor

> Documentation provided by [Postman](https://documenter.getpostman.com/view/153390/sensehat/7Lt5eX1)