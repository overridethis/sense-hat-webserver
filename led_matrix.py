#!flask/bin/python
from flask import Blueprint, request, jsonify
from sense_hat import SenseHat

led_matrix = Blueprint('led_matrix', __name__)
hat = SenseHat()

@led_matrix.route('/')
def index():
    return "SenseHat API!"

@led_matrix.route('/hat/led/clear', methods=['DELETE'])
def clear():
    j = request.get_json()
    if (j):
        r = j['r'] or 0
        g = j['g'] or 0
        b = j['b'] or 0
        hat.clear((r,g,b))
    else:
        hat.clear()
    return '', 204

@led_matrix.route('/hat/led/set_rotation', methods=['POST'])
def set_rotation():
    j = request.get_json()
    r = j['r'] or 0
    redraw = True if j['redraw'] else False
    hat.set_rotation(r, redraw)
    return '', 200

@led_matrix.route('/hat/led/flip_h', methods=['POST'])
def flip_h():
    j = request.get_json()
    if (j):
        redraw = True if j['redraw'] else False
        hat.flip_h(redraw)
    else:
        hat.flip_h(True)
    return '', 200

@led_matrix.route('/hat/led/flip_v', methods=['POST'])
def flip_v():
    j = request.get_json()
    if (j):
        redraw = True if j['redraw'] else False
        hat.flip_v(redraw)
    else:
        hat.flip_v(True)
    return '', 200

@led_matrix.route('/hat/led/set_pixels', methods=['POST'])
def set_pixels():
    j = request.get_json()
    hat.set_pixels(j['pixel_list'])
    return '', 200

@led_matrix.route('/hat/led/get_pixels', methods=['GET'])
def get_pixels():
    p = hat.get_pixels()
    return jsonify(p), 200

@led_matrix.route('/hat/led/set_pixel', methods=['POST'])
def set_pixel():
    j = request.get_json()
    x = j['x'] or 0
    y = j['y'] or 0
    r = j['r'] or 0
    g = j['g'] or 0
    b = j['b'] or 0
    hat.set_pixel(x,y,(r,g,b))
    return '', 200

@led_matrix.route('/hat/get_pixel', methods=['POST'])
def get_pixel():
    j = request.get_json()
    x = j['x'] or 0
    y = j['y'] or 0
    return jsonify(hat.get_pixel(x,y)), 200

@led_matrix.route('/hat/led/show_letter', methods=['POST'])
def show_letter():
    j = request.get_json()
    s = j['s']
    text_colour = (j['text_colour']['r'] or 255, j['text_colour']['g'] or 255, j['text_colour']['b'] or 255)
    back_colour = (j['back_colour']['r'] or 0, j['back_colour']['g'] or 0, j['back_colour']['b'] or 0)
    hat.show_letter(s,text_colour,back_colour)
    return '',200

@led_matrix.route('/hat/led/show_message', methods=['POST'])
def show_message():
    j = request.get_json()
    hat.show_message(j['msg'] or "NULL")
    return '', 200

@led_matrix.route('/hat/led/low_light', methods=['POST','DELETE'])
def low_light():
    hat.low_light = request.method == 'POST'
    s = (200 if request.method == 'POST' else 204)
    return '', s